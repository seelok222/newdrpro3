import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingpageComponent } from './modules/landingpage/landingpage.component';

const routes: Routes = [
  { path: '', component: LandingpageComponent, },
  {
    path: 'landing-page',
    loadChildren: () => import('./modules/landingpage/landingpage.module')
      .then(mod => mod.LandingpageModule)
  },
  {
    path: 'gallery-detail',
    loadChildren: () => import('./modules/gallery-detail/gallery-detail.module')
      .then(mod => mod.GalleryDetailModule)
  },
  {
    path: 'student-detail',
    loadChildren: () => import('./modules/student-detail/student-detail.module')
      .then(mod => mod.StudentDetailModule)
  },
  {
    path: 'news-detail',
    loadChildren: () => import('./modules/news-detail/news-detail.module')
      .then(mod => mod.NewsDetailModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
