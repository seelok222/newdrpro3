import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GalleryDetailRoutingModule } from './gallery-detail-routing.module';
import { GalleryDetailComponent } from './gallery-detail.component';


@NgModule({
  declarations: [
    GalleryDetailComponent
  ],
  imports: [
    CommonModule,
    GalleryDetailRoutingModule
  ]
})
export class GalleryDetailModule { }
