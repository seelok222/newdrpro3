import { componentFactoryName } from '@angular/compiler';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GalleryDetailComponent } from './gallery-detail.component';
import { GalleryDetailModule } from './gallery-detail.module';

const routes: Routes = [
  {
    path:'', component:GalleryDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GalleryDetailRoutingModule { }
