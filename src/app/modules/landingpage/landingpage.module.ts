import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingpageRoutingModule } from './landingpage-routing.module';
import { LandingpageComponent } from './landingpage.component';
import { AboutComponent } from './about/about.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { GalleryComponent } from './gallery/gallery.component';
import { StudentComponent } from './student/student.component';
import { PerfomanceComponent } from './perfomance/perfomance.component';
import { EntertainerComponent } from './entertainer/entertainer.component';
import { NewsComponent } from './news/news.component';
import { SlideComponent } from './slide/slide.component';
import { ContactComponent } from './contact/contact.component';
import { GalleryDetailModule } from '../gallery-detail/gallery-detail.module';


@NgModule({
  declarations: [
    LandingpageComponent,
    AboutComponent,
    PortfolioComponent,
    GalleryComponent,
    StudentComponent,
    PerfomanceComponent,
    EntertainerComponent,
    NewsComponent,
    SlideComponent,
    ContactComponent
  ],
  imports: [
    CommonModule,
    LandingpageRoutingModule,
    GalleryDetailModule
  ]
})
export class LandingpageModule { }
